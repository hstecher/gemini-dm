# gemini-dm

Gemini Workstation MK container

The gemini-dm container runs centos 7 with gemini-workstation-mk.rpm installed which allows you to run dm screens from a container. 

Note: this is a very large container (almost 6GB) it contains all of the dependencies in gemini-workstation-mk.rpm, many of which are not needed for dm screens.

gitLab registry login:
docker login registry.gitlab.com -u <username> -p <personal access token>

macOS:
-install docker desktop
-install and run XQuartz
-check XQuartz->preferences->Security->Allow connections from network clients
-xhost + 

-download/start container (Note: the container image will need to download the first time you run it.)

docker run -it --rm --net=host --dns-search hi.gemini.edu --name gem-dm -e DISPLAY=host.docker.internal:0 registry.gitlab.com/hstecher/gemini-dm bash

Stan's office AG
docker run --rm -it --net=host --dns-search hi.gemini.edu --name gem-dm -e DISPLAY=host.docker.internal:0 -v /Users/hstecher/work/dockerfiles/gemini-dm/stan_ag:/tmp/ag_test registry.gitlab.com/hstecher/gemini-dm bash

-start dm screens from the command line


Build image from Dockerfile:

-You will need the tktable rpm from this repository in your working directory. It is modified to build with the available public packages. Otherwise you will need to configure the gemini mirror repos.
-Once the image is build start a container and rsync /gemsoft/etc from a real telops machine into the contianer:/gemsoft/etc

Future work:
-dns name resolution isn't setup. scripts that use dns names will not work
-tcc, iocstats don't work, probably because of dns
-create a non root user to run dm screens 


