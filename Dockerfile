FROM centos:7
ENV container docker
STOPSIGNAL SIGRTMIN+3

RUN export GEMINI_SITE=MK

RUN yum -y install epel-release

RUN yum -y groupinstall "Development Tools"
RUN yum -y install sssd-client
RUN yum -y install sudo

RUN yum-config-manager --add-repo=http://hbfisglinux1.hi.gemini.edu/yum/gemini/rhel/7/production/x86_64/

COPY tktable-2.10-12.el5.x86_64.rpm /tmp
RUN yum localinstall -y /tmp/tktable-2.10-12.el5.x86_64.rpm

RUN yum  --nogpgcheck  --setopt=protected_multilib=false install -y gemini-workstation-mk

RUN yum clean all

CMD ["/sbin/init"]

